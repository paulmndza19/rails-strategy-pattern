function sort(){
    sortType = document.getElementById("sort-type").value;
    inputString = document.getElementById("input-string").value;

    // Input Validations
    if (sortType == "" && inputString == "" ) {
        alert("Please provide the necessary details")
        return
    }

    if (sortType == "") {
        alert("Please select a sorting strategy")
        return
    }

    if (inputString == "") {
        alert("Please provide a string to sort")
        return
    }

    // Call the sort_string endpoint
    fetch('/sort_string?input_string='+inputString+'&sort_type='+sortType)
    .then(function(response) {
        return response.json()
    })
    .then(function(data) {
        // Display Result
        document.getElementById("result").textContent = data.data
    });
}