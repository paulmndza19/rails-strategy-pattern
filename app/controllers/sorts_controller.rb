class SortsController < ApplicationController
    include SortStrategy
    skip_before_action :verify_authenticity_token

    def index
    end

    def sort_string
        # Return an error if sort type and input string is blank
        render :json => { error: "Sort Type is required" } and return if params[:sort_type].blank?
        render :json => { error: "Input string is required" } and return if params[:input_string].blank?
        # Strategy
        strategy = (params[:sort_type] == "bubble") ? SortStrategy::BubbleSort : SortStrategy::QuickSort
        # Call the sort function in the Sort model
        render :json => { data: Sort.sort_string(params[:input_string], strategy ) }
    end      

end