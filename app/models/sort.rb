class Sort 
    def self.sort_string(input_string, strategy)
        # Convert the string into array for it to be sorted
        array = input_string.split('')
        # Call the specified strategy
        result = strategy.sort(array)
        # If there is an error in the processing of the string it will return a generic error
        # else it will return the string
        if result.class == String
            return result
        else
            return result.join()
        end
    end
end