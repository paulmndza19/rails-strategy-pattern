module SortStrategy

    class BubbleSort
        def self.sort(array)
            begin
                # If array has only one value return the array itself
                return array if array.size <= 1
                swap = true
                # While swap is true compare all values of the array
                while swap
                    swap = false # Without this line of code this will be an infinite loop
                    (array.length - 1).times do |x|
                        # Compare selected items
                        if array[x] > array[x+1]
                            # Swap values if array[x] is greater than array[x+1]
                            array[x], array[x+1] = array[x+1], array[x]
                            # Return swap to true in order to continue loop
                            swap = true
                        end
                    end
                end
                return array
        
            rescue => exception
                return "There was an error in processing your input. Please make sure to input a correct string"
            end
        end
    end
    
    class QuickSort
        def self.sort(array)
            begin
                # If array has only one value return the array itself
                return array if array.length <= 1
                # Select a random pivot to start the quick sort
                pivot = array.delete_at(rand(array.length))
                
                # Declare arrays that will be the container of the compared values
                left = Array.new # for values less than the pivot
                right = Array.new # for values greater than the pivot
                
                # If x is less than or equal to pivot then insert it into the left side otherwise insert it into the right side
                array.each do |x|
                    if x <= pivot
                        left << x 
                    else
                        right << x
                    end
                end

                # Call the quick_sort function for the values on the left side and right side of the pivot
                return *self.sort(left), pivot, *self.sort(right)
            rescue => exception
                return "There was an error in processing your input. Please make sure to input a correct string"
            end
        end
    end

end