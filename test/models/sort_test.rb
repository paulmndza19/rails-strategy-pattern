require 'test_helper'

class SortTest < ActiveSupport::TestCase
    # Test Bubble Sort
    test "sort string using bubble sort" do
        result = Sort.sort_string("bubble", "befdac") == "abcdef"
        refute result.nil?
    end

    # Test Quick Sort
    test "sort string using quick sort" do
        result = Sort.sort_string("quick", "befdac") == "abcdef"
        refute result.nil?
    end

end
