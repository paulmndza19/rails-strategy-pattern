Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "sorts#index" # Set the root url to the sort page
  get "sorts" => "sorts#index"
  get "sort_string" => "sorts#sort_string"
end
